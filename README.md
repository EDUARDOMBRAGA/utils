# Comands Utils

## USING COMMANDS AT DOCKER

- Restart service docker

`$ service docker restart`

- To create the containers listed in a .yml file (Docker-compose)

`$ docker-compose up -d`

- To view the container execution log

`$ docker-compose logs`
`$ docker-compose logs '<<name of the service>>'`


- Restarting all running docker containers
`$ docker restart $(docker ps -a -q)`

- Remove containers and images at docker
[Tutorial](https://linuxize.com/post/how-to-remove-docker-images-containers-volumes-and-networks/)

- Commands docker-compose
[Commands](/Docker-compose/docker-compose.yml)
## USING SEQUELIZE-CLI CONTROLS

- To create a new migration

`$ sequelize migration:generate --name [name_of_your_migration]`

- To run all migrations

`$ node_modules/.bin/sequelize db:migrate`
// or if your sequelize is used within node modules folder
`$ sequelize db:migrate`


- List applied sequelize migrations.

`$ sequelize migrate:status`
// or if your sequelize is used within node modules folder
`$ ./node_modules/.bin/sequelize migrate:status`

https://www.npmjs.com/package/sequelize-cli